from django.urls import path
from receipts.views import (
    receipts_list_view,
    create_receipt_listview,
    category_list,
    create_account,
    account_list,
    create_category,
)

urlpatterns = [
    path("receipts/", receipts_list_view, name="home"),
    path("receipts/create/", create_receipt_listview, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path(
        "receipts/categories/create/", create_category, name="create_category"
    ),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
]
